public class List {
    private ListElement head;       // указатель на первый элемент
    private ListElement tail;       // указатель на последний элемент


    void addBack(int busNumber, String driverName,int marNumber)       //добавление в конец списка
    {
        ListElement a = new ListElement();  //создаём новый элемент
        a.busNumber = busNumber;
        a.driverName = driverName;
        a.marNumber = marNumber;


        if (tail == null)           //если список пуст
        {                           //то указываем ссылки начала и конца на новый элемент
            head = a;               //т.е. список теперь состоит из одного элемента
            tail = a;
        } else {
            tail.next = a;          //иначе "старый" последний элемент теперь ссылается на новый
            tail = a;               //а в указатель на последний элемент записываем адрес нового элемента
        }
    }
    
    void printList()                //печать списка
    {
        ListElement t = head;       //получаем ссылку на первый элемент
        while (t != null)           //пока элемент существует
        {
            System.out.print("Автобус:  " + t.busNumber + " Водитель:  " + t.driverName + " Номер маршрута " + t.marNumber  + "\n"); //печатаем его данные
            t = t.next;                     //и переключаемся на следующий
        }
    }


    void search ( int busNumber1) //удаление элемента
    {
        if (head == null) {
            System.out.println(" Данный автобус отсутствует в парке.");
            return;
        }

        if (head.busNumber == busNumber1) {    //если первый элемент - тот, что нам нужен

            head.marNumber -= 1;
            return;                 //и выходим
        }

        ListElement t = head;       //иначе начинаем искать
        while (t.next != null) {    //пока следующий элемент существует
            if (t.next.busNumber == busNumber1) {  //проверяем следующий элемент
                if(tail == t.next)      //если он последний
                {
                    head.marNumber -= 1;          //то переключаем указатель на последний элемент на текущий
                }
            }
            t = t.next;                //иначе ищем дальше
        }
    }
    void delEl(int busNumber) //удаление элемента
    {
        if(head == null) //если список пуст -
            return; //ничего не делаем
        if (head == tail) { //если список состоит из одного элемента
            head = null; //очищаем указатели начала и конца
            tail = null;
            return; //и выходим

        }
        if (head.busNumber == busNumber) { //если первый элемент - тот, что нам нужен
            head = head.next; //переключаем указатель начала на второй элемент
            return; //и выходим
        }
        ListElement t = head; //иначе начинаем искать
        while (t.next != null) { //пока следующий элемент существует
            if (t.next.busNumber == busNumber) { //проверяем следующий элемент
                if(tail == t.next) //если он последний
                {
                    tail = t; //то переключаем указатель на последний элемент на текущий
                }

                t.next = t.next.next; //найденный элемент выкидываем

                return; //и выходим
            }
            t = t.next; //иначе ищем дальше
        }


    }



}
