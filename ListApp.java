import java.util.Scanner;
public class ListApp {


    public static void main(String[] args) {
        List theBusList = new List(); // Создание нового списка
        List marList = new List();
        int choice = 0;


        {
            while (choice != 7) {
                Scanner scanner = new Scanner(System.in);
                System.out.println(" ");
                System.out.println("Bus control");
                System.out.println("Меню");
                System.out.println("1) Добавить новый автобус.");
                System.out.println("2) Выезд автобуса на маршрут:");
                System.out.println("3) Въезд автобуса в парк:");
                System.out.println("4) Вывести все автобусы, которые в парке: ");
                System.out.println("5) Вывести все автобусы, которые на маршруте: ");
                System.out.println("\n6) Выйти из программы:");
                System.out.println("> ");
                choice = scanner.nextInt();
                switch (choice) {
                    case 1:
                        System.out.println("Введите номер автобуса: ");
                        int busNumber = scanner.nextInt();
                        System.out.println("Введите фамилию и инициалы водителя: ");
                        String driverName = scanner.next();
                        System.out.println("Введите номер маршрута: ");
                        int marNumber = scanner.nextInt();
                        System.out.println("Добавлена: " + "номер автобуса:  " + busNumber + " Водитель:  " + driverName + " Номер маршрута " + marNumber  );
                        theBusList.addBack(busNumber, driverName, marNumber);
                        break;
                    case 2:
                        System.out.println("Номер автобуса: ");
                        int busNumber1 = scanner.nextInt();
                        System.out.println("Выехал автобус с номером: " + busNumber1);
                        theBusList.delEl(busNumber1);
                        break;
                    case 3:
                        System.out.println("Номер автобуса: ");
                        int busNumber2 = scanner.nextInt();
                        System.out.println("Въехал автобус с номером: " + busNumber2);
                        theBusList.search(busNumber2);
                        break;
                    case 4:
                        System.out.println(" ");
                        System.out.println("Вывести все автобусы, которые в парке: ");
                        theBusList.printList();
                        System.out.println(" ");
                        break;
                    case 5:
                        System.out.println(" ");
                        System.out.println("Вывести все автобусы, которые  на маршруте: ");
                        System.out.println(" ");

                }
                System.out.println("Вышел из программы");
            }
        }
    }
}
